%define apache_serverroot /usr/share
%define apache_confdir /etc/httpd/conf.d
%define nc_dir  %{apache_serverroot}/nextcloud
%define nc_data_dir     %{nc_dir}/data

%define nc_user apache
%define nc_group apache

# Turn off the brp-python-bytecompile script
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')


Summary: Nextcloud package
Name: nextcloud
Version: 20.0.11
Release: 1%{?dist}
License: GPL
Source0: %{name}-%{version}.tar.bz2
Source1: nextcloud.conf
Source2: config.php
BuildArch: noarch
URL: https://nextcloud.com/

Obsoletes: nextcloud-httpd  <= 19.0.0
Obsoletes: nextcloud-mysql  <= 19.0.0
Obsoletes: nextcloud        <= 19.0.0

Patch1: nextcloud.patch
BuildRequires: httpd

Requires: httpd
# Required php packages
Requires: rh-php73
Requires: rh-php73-php-fpm
Requires: rh-php73-php-gd
Requires: rh-php73-php-pdo
Requires: rh-php73-php-mbstring

# Recommended php packages
Requires: rh-php73-php-intl

# Required php packages for specific apps
Requires: rh-php73-php-ldap

# Required php packages for MariaDB
Requires: rh-php73-php-pdo_mysql


%description
Nextcloud files and configuration.

This package installs as follows:
nc_dir:        %{nc_dir}
nc_data_dir:   %{nc_data_dir}

%prep
%setup -q -n %{name}
%patch1 -p1

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{nc_data_dir}
mkdir -p %{buildroot}%{nc_dir}/assets

cd ..
cp -r %{name} %{buildroot}/usr/share
install -D %{SOURCE1} %{buildroot}%{apache_confdir}/nextcloud.conf
install -D %{SOURCE2} %{buildroot}%{_sysconfdir}/nextcloud/config.php
touch %{buildroot}%{_sysconfdir}/nextcloud/CAN_INSTALL

rm -rf %{buildroot}%{nc_dir}/config

# symlink config dir
ln -sf %{_sysconfdir}/nextcloud %{buildroot}/%{nc_dir}/config

%files 
%{nc_dir}/config
%defattr(0640,root,%{nc_group},0750)
%dir %attr(0755,root,%{nc_group}) %{nc_dir}
%dir %attr(0775,root,%{nc_group}) %{_sysconfdir}/nextcloud
%attr(0755,%{nc_user},%{nc_group}) %{nc_dir}/occ
%attr(0750,%{nc_user},%{nc_group}) %{nc_dir}/apps
%attr(0750,%{nc_user},%{nc_group}) %{nc_dir}/assets
%attr(0750,%{nc_user},%{nc_group}) %{nc_dir}/updater
%attr(0775,%{nc_user},%{nc_group}) %{nc_data_dir}
%attr(0755,%{nc_user},%{nc_group}) %{nc_dir}/lib
%attr(0755,%{nc_user},%{nc_group}) %{nc_dir}/core
%attr(0755,%{nc_user},%{nc_group}) %{nc_dir}/3rdparty
%attr(0755,%{nc_user},%{nc_group}) %{nc_dir}/resources
%attr(0755,%{nc_user},%{nc_group}) %{nc_dir}/themes
%attr(0755,%{nc_user},%{nc_group}) %{nc_dir}/ocs*
%attr(0755,%{nc_user},%{nc_group}) %{nc_dir}/*.php
%attr(0755,%{nc_user},%{nc_group}) %{nc_dir}/AUTHORS
%attr(0755,%{nc_user},%{nc_group}) %{nc_dir}/index.html
%attr(0755,%{nc_user},%{nc_group}) %{nc_dir}/robots.txt
%attr(0644,%{nc_user},%{nc_group}) %{nc_dir}/.htaccess
%attr(0644,%{nc_user},%{nc_group}) %{nc_dir}/COPYING
%attr(0644,%{nc_user},%{nc_group}) %{nc_dir}/ocm-provider/index.php

%config(noreplace) %attr(0640,%{nc_user},%{nc_group}) %{_sysconfdir}/nextcloud/config.php
%config(noreplace) %attr(0640,%{nc_user},%{nc_group}) %{_sysconfdir}/nextcloud/CAN_INSTALL
%config(noreplace) %attr(0644,%{nc_user},%{nc_group}) %{nc_dir}/.user.ini
%config(noreplace) %attr(0644,root,root) %{apache_confdir}/nextcloud.conf

%defattr(0644,%{nc_user},%{nc_group},0755)


%changelog
* Thu Jul 01 2021 Tyler Randolph <trandolph@clearcenter.com>
- Bumped to version 20.0.11
* Thu Jun 04 2020 Tyler Randolph <trandolph@clearcenter.com>
- Bumped to version 19.0.0
* Thu May 07 2020 Tyler Randolph <trandolph@clearcenter.com>
- Bumped to version 18.0.4
* Fri Mar 20 2020 Tyler Randolph <trandolph@clearcenter.com>
- Bumped to version 18.0.2
* Wed Jan 22 2020 Tyler Randolph <trandolph@clearcenter.com>
- Bumped to version 18.0.0
* Thu Oct 31 2019 Tyler Randolph <trandolph@clearcenter.com>
- Bumped to version 17.0.0
* Mon Aug 19 2019 Tyler Randolph <trandolph@clearcenter.com>
- Bumped to version 16.0.4
* Mon Jun 03 2019 Nick Howitt <nhowitt@clearcenter.com>
- revamp spec file to use patched sources
- fix file listed twice warning
- where parameters existed in the spec file, make sure they were used
- prefer "install -D" over "mkdir" and "cp"
* Fri Apr 26 2019 Tyler Randolph <trandolph@clearcenter.com>
- Bumped to version 15.0.7 and fixed file listed twice error
* Fri Aug 24 2018 Tyler Randolph <trandolph@clearcenter.com>
- Fixed folder permissions
* Tue Aug 21 2018 Benjamin Chambers <bchambers@clearfoundation.com>
- Bumped to version 13.0.5 and moved config to /etc/nextcloud
* Wed Jun 13 2018 Benjamin Chambers <bchambers@clearfoundation.com>
- Original spec sourced from https://github.com/nextcloud/server-packages - modified for ClearOS
